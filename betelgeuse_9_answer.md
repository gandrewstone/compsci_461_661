I've introduced you to a friend who is an alien from the planet Betelgeuse-9. They don't use numbers like we do! They use an ordered system of symbols as follows: dot $\bullet$, square $\blacksquare$,  chi $\chi$, triangle $\blacktriangle$, circle $\circ$, clubs $\clubsuit$, diamond $\diamondsuit$, dash $-$, and heart~$\heartsuit$.  This system also has an operator ``+".  

I tell the alien that as a shorthand, we can also define a scalar ``multiply" operation n*S  (where n is a scalar, and S is a symbol in the group) which just means S+S+...+S (n times).

$\blacksquare \oplus \blacksquare = 2 \otimes \blacksquare = \chi$
$\blacksquare \oplus \blacksquare \oplus \blacksquare = 3 \otimes \blacksquare = \blacktriangle$
$\blacksquare \oplus \blacksquare \oplus \blacksquare \oplus \blacksquare  = 4 \otimes \blacksquare = \circ$
and so on. 

Also note that $\bullet \oplus \blacksquare = \blacksquare$, and $\heartsuit \oplus \blacksquare = \bullet$

This is an Abelian (i.e., commutative) group.  Don't be confused by these alien symbols!  Remember that a group follows simple rules, and the algorithms we discussed in class apply to any commutative group.  

**Hint, perhaps finish the table of how many squares sum to each group element to make multiplication simple.**
$1 \blacksquare = \blacksquare$
$2 \blacksquare = \chi$
$3 \blacksquare = \blacktriangle$
$4 \blacksquare = \circ$
$5 \blacksquare = \clubsuit$
$6 \blacksquare = \diamondsuit$
$7 \blacksquare = -$
$8 \blacksquare = \heartsuit$
$9 \blacksquare = \bullet$

*What special role does the square symbol play in defining this  group?  How about the dot?

Square is the group generator.  Dot is the identity element.

*Demonstrate to the alien the Diffie Helman algorithm between two participants, Alice and Bob, using this alien's group.  Use 3 for Alice's private key, and 5 for Bob's private key.*

$AlicePub = 3\blacksquare = \chi$
$BobPub = 5\blacksquare = \clubsuit$

Alice sends $\chi$ to Bob, Bob sends clubs to Alice.

Alice computes $3\clubs$, Bob computes 5$\chi$, 

$3\clubs = 5\chi = 15\blacksquare$

What is Alice and Bob's shared key based on your calculations in part (b) above?

$3\clubs = 5\chi = 15\blacksquare = \diamondsuit$

Its diamond because 15mod9=6 (look up 6 square in your table).


*Why isn't this system secure?*

Operation is reversible, group is so small it can be brute-forced.