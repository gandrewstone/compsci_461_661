# CompSci 461/661

## General
**Instructor:** G. Andrew Stone (gastone@umass.edu): Office Hours on Tues by appointment (send a message on Piazza) or via Zoom (813 149 9032) any day most times (PM me on Piazza).

[Syllabus](/policies/syllabus_461_661.pdf)

**TAs:** 
Ojaswi Acharya   oacharya@umass.edu

**Graders:** 
Srimathi Mahalingam srimathimaha@umass.edu
Harsh Seth hseth@umass.edu
Megha Singh meghasingh@umass.edu
Yash Kothekar ykothekar@umass.edu

**Classroom**:   
461: Tues 2:30PM - 3:45PM: Computer Sciences Building 140
661: Tues 4:00PM - 5:15PM: Computer Sciences Building 140 (remote students use Zoom meeting ID 953 3538 4635)

**Discussion: [Piazza](https://piazza.com/class/m0l4oowal524ly)**
**Assignment Submission: [Gradescope](https://www.gradescope.com/courses/852131)**  Course Entry Code: 6J7VK4
**Midterm: Oct 29 Midterm In class**
**Final: TBD**

### ****General Reference Materials:****

[**Bitcoin (UTXO) blockchain Concepts**](bitcoinRefMaterials.md)
[**Math**](mathRefMaterials.md)
[**Ethereum**](ethereumRefMaterials.md)

## In Class Notes and Recordings

[Week N notes](/nodes/weeknotes)
[461 Recording](https://echo360.org/section/39f0cab0-02c1-41c5-b5c0-f3f6b5116f93/home)
[661 Recording](https://echo360.org/section/c53bd5a9-7d3b-4d22-85ce-c71b6f39c379/home)

[Schnorr signatures](/notes/schnorr2022.pdf)
[Diffie Hellman Key Exchange](/notes/diffie_hellman_on_groups.pdf)


## Weekly Process
Each week we'll follow the same pattern:

1. Skim the PDF materials.
2. Watch the video, which is an overview of the PDFs. It does not replace the PDFs!
3. Read through the PDF in detail
4. Come to class prepared for discussion with questions remaining from what you read, and ready to answer questions from your instructor. Discussion is not a repeat of the videos.  It's a discussion.  Attendance is mandatory!

# Week 14 (Dec 3-10)  Blockchain applications
- [CoinJoin](https://en.bitcoin.it/wiki/CoinJoin)  (Anonymity)
- [Lightning Video Lecture](/week/lightning/lightning.mp4)
- [Blockchain Voting Techniques](https://github.com/bitcoin-unlimited/BUwiki/blob/master/blockchain_voting.md)
- [Voting's impossible triangle](https://github.com/bitcoin-unlimited/BUwiki/blob/master/election_impossible_triangle.md)

# Week 13: (Nov 19-Dec 3)
 - [Constant Function Market Maker](https://medium.com/bollinger-investment-group/constant-function-market-makers-defis-zero-to-one-innovation-968f77022159) (Trustless, permissionless, exchange technique)
- Bitcoin Script Virtual Machine Description: https://github.com/bitcoinbook/bitcoinbook/blob/develop/ch07_authorization-authentication.adoc
- [Bitcoin (actually Nexa) instruction set (opcode) Reference](https://spec.nexa.org/script/1script/).  Yes, actually READ through these!  (so you get a sense of what can and can't be done)

- [Ethereum is a dark forest](https://www.paradigm.xyz/2020/08/ethereum-is-a-dark-forest) (Ethereum development story)

### Reference
- [Bitcoin Scripts and Design Patterns](/week/transactions/example_scripts.pdf)

## Week 12: (Nov 12-19)   Byzantine Generals Problem

[Byzantine Generals Video Lecture](/week/byzantine_generals/Byzantine_Generals.mp4)
[Byzantine Fault Tolerance vs Proof Of Work Video Lecture](/week/byzantine_generals/BFT_POW_Comparison.mp4)

[Vukolic paper: Proof-of-work vs. BFT Replication](/papers/iNetSec_2015.pdf)

### Reference

[Byzantine Generals Video Lecture Notes](/week/byzantine_generals/Byzantine_Generals.pdf)
[FLP, CAP, BFT Comparison lecture notes](/week/byzantine_generals/BFT_POW_Comparison.pdf)

[(optional) Impossibility Of Distributed Consensus (original FLP paper)](/papers/Impossibility_Distributed_Consensus_FLP_jacm85.pdf)




## Week 11: (Nov 5-12)  Ethereum-family Scripting, Bitcoin-family Scripting, Tokens

### Tools
- Ethereum online IDE: https://remix.ethereum.org
- Bitcoin Scripting online: https://siminchen.github.io/bitcoinIDE/build/editor.html
- Nexa script debugger: https://debug.nexa.org/

### Reference material
- Ethereum Tokens
	- Contract-implemented and enforced tokens (similar to what you did in cryptozombies).  Follow the link into the EIPs to see the Solidity interface.
	 - [ERC-20 Token Standard](https://ethereum.org/en/developers/docs/standards/tokens/erc-20/)
	 - [ERC-721 Non-fungible Tokens](https://ethereum.org/en/developers/docs/standards/tokens/erc-721/)
	 
- Bitcoin Tokens (Ordinals and Inscriptions)
	- https://docs.ordinals.com/overview.html and/or https://github.com/ordinals/ord/blob/master/bip.mediawiki Skim to understand that one can assign each Satoshi an order by mining -- this is the Nth satoshi mined, and that that number can be carried forward through transactions to assign a number to every satoshi in the UTXO set.

- [Native Blockchain Tokens (Group Tokenization Proposal)](https://docs.google.com/document/d/1X-yrqBJNj6oGPku49krZqTMGNNEWnUJBRFjX7fJXvTs/edit?usp=sharing)  
	 - Miner validated tokens on bitcoin-like blockchains.  Skip the intro, read "Functional Description", up to "Contract Encumbered Groups".  Read the use cases to understand general token use cases for any token technology on any blockchain.


 ## Week 10: (Nov 5) No class (election day)

 ## Week 9: (Oct 29) Midterm In class

 ## Week 8: (Oct 22) Midterm Review
Bring lots of questions

[Tricky question about group theory](betelgeuse_9.md)   -----   [answer](betelgeuse_9_answer.md)

 Continue Cryptozombies: https://cryptozombies.io/en/solidity
 (Do not do "App Front-ends & Web3.js", unless you want to!)
 
 ## Week 7: (Oct 15) No class (follow monday's schedule) Do CryptoZombies!
 Head over to this site and work through the first 5 lessons:
 https://cryptozombies.io/en/solidity
 (Do not do "App Front-ends & Web3.js", unless you want to!)

## Week 6: (Oct 1 - Oct 8) :  Block Propagation

[Efficient Block Propagation Lecture Video](/week/block_propagation/graphene_lecture.mp4)

[Bloom Filter Lecture Notes](/week/block_propagation/bloom_filters_notes.pdf)
[Invertible Bloom Lookup Table (IBLT)](/papers/IBLT_1101.2245.pdf)
[(661) Graphene Paper](/papers/graphene.sigcomm.pdf)
[(461) Graphene short paper](/week/block_propagation/graphene_short.pdf)


## Week 5: (Sep 24 - Oct 1) Bitcoin Networking, Eclipse Attacks, and Distributed Clock Synchronization

- [Networking and Eclipse Attack Video Lecture](/week/networking_eclipse/Networking_Eclipse.mp4)
- [Single transaction block analysis](/papers/1txn.pdf)

- **Clock Synchronization**

We are going to use short excerpts from "Distributed Systems: Principles and Paradigms" by Andrew S. Tanenbaum and Maarten Van Steen. Fortunately, the entire text is available for free! Just give any email address to the [author's web site](https://www.distributed-systems.net/index.php/books/ds3/) and they'll send you a PDF.

Please read:
-   Section 6.1: Clock Synchronization (no need to read the Berkeley algorithm or about wireless networks)
-   Section 6.2: Logical clocks

Later in the semester, we'll use the text for another topic.

- [Logical Clock Conditions - Georgia Tech](https://www.youtube.com/watch?v=gaptoWIYgeg)
- [Real World Scenario (clocks) - Georgia Tech](https://www.youtube.com/watch?v=HX_EA8_nG54)

### Reference
 - [**Eclipse Attacks on Bitcoin's Peer-to-Peer Network**](/papers/sec15-paper-heilman.pdf)

Purely optional reading for the course, but if you are interested, here is the original Lamport paper:
-   [**Time, clocks, and the ordering of events in a distributed system**](http://amturing.acm.org/p558-lamport.pdf), by Leslie Lamport, Communications of the ACM; 21(7): 558–565. (1978)


## Week 4: (Sep 17 - 24) Elliptic Curve Crypto

- [ECC Video Lecture part 1](/week/elliptic_curve_crypto/ECC_part1.mp4)
- [ECC Video Lecture part 2](/week/elliptic_curve_crypto/ECC_part2.mp4)

**661 only**
Review this paper [Attacks against Autofinalization and Parking](/papers/autofinalization_parking.pdf).
Understand:
 * That a variety of "tweaks" can be invented to modify Satoshi's algorithm to choose the "main chain"
 * What two proposed tweaks: "autofinalization" and "parking" actually do
 * There seems to be no description of this consensus change, except for the [source code located in this function](https://github.com/Bitcoin-ABC/bitcoin-abc/blob/7e8aab945660cbb148bea1db029300568c543e68/src/validation.cpp#L2567) (reading just the comments may be helpful).

### Reference
[(Optional) elliptic-curve-cryptography-a-gentle-introduction](https://andrea.corbellini.name/2015/05/17/elliptic-curve-cryptography-a-gentle-introduction/)
[(Optional) ECC part 2](https://andrea.corbellini.name/2015/05/23/elliptic-curve-cryptography-finite-fields-and-discrete-logarithms/)
[(Optional) Chapter 8, Paar and Pelzl](/papers/paar_pelzl_chapter_8_excerpt.pdf)
[(Optional) Chapter 9, Paar and Pelzl](/papers/paar_pelzl_chapter_9.pdf)

## Week 3: (Sep 10 - 17) Selfish Mining, Sybil Attack
Please skim the papers, watch the videos, and then read the papers carefully. 
- [Selfish Mining paper](/papers/selfish_mining-btcProcFC.pdf )
- [Sybil Attack paper](/papers/sybil_attack-IPTPS2002.pdf)

- [Sybil Attack Lecture](/week/sybil/Sybil_Attacks.mp4)
- [Selfish Mining Lecture](/week/selfish/Selfish_Mining.mp4)

### Reference
- [Sybil Attack Lecture Notes](/week/sybil/Sybil_Attacks.pdf)
- [Selfish Mining Lecture Notes](/week/selfish/Selfish_Mining.pdf)


## Week 2: (Sep 3-10) Cryptography Overview, Doublespend Attacks 

We continue our investigation into basic blockchain architecture.   
  
- [Overview of Applied Cryptography Video Lecture](/week/crypto_overview/cryptography_intro.mp4)
-  [Doublespend Attack Video Lecture (1/2)](/week/doublespend_attack/doublespend_intro.mp4)
-  [Doublespend Attack  (2/2)](/week/doublespend_attack/doublespend_analysis.mp4) -- note the final equation in this video has some typos although the derivation is correct in essence.  Use the equation in Satoshi's white paper to calculate the likelihood of matching the main chain.  And to calculate the probability of exceeding the main chain:

$$\lambda = \frac{zq}{p}$$
$$1- \sum_{k=0}^{z+1} (\frac{\lambda^ke^{-\lambda}}{k!})(1-(q/p)^{z+1-k}), if q < p$$

### Reference
- [Overview of Applied Cryptography Notes](/week/crypto_overview/overview_of_applied_cryptography.pdf) 
- [Doublespend Analysis Notes](/week/doublespend_attack/doublespend_analysis.pdf) -- with correct doublespend derivation 
- [basic probability](/notes/simple_probability.pdf)


## Week 1: Introduction
These are the materials for our first week.

- [The Bitcoin Whitepaper](/papers/Bitcoin-satoshi.pdf)
- [Introduction  and Class Logistics Video Lecture](/week/introduction/intro-definitions.mp4)
- [Blockchains Part 1 Video Lecture](/week/introduction/blockchains_part_1.mp4)
- [Blockchains Part 2 Video Lecture](/week/introduction/blockchains_part_2.mp4)

### Reference
- [Introduction  and Class Logistics Video Notes](/week/introduction/introduction_and_class_logistics.pdf)
- [Blockchains Video Notes](/week/introduction/Bitcoin_and_Nakamotos_paper.pdf)
- [blockchain in one diagram](/notes/blockchain_in_one_diagram.pdf)
- test