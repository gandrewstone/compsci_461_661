# CompSci 461/661

## General
**Instructor:** G. Andrew Stone (gastone@umass.edu)

[Syllabus](/policies/syllabus_461_661.pdf)

**TA:** TBD
**Graders:** TBD

**Classroom**:   
661: TBD
461: TBD


**Discussion: [Piazza](https://piazza.com/class/TBD)**

**Gradescope: https://www.gradescope.com/courses/TBD**
**Midterm: TBD**
**Final: TBD**

### ****General Reference Materials:****

[**Bitcoin (UTXO) blockchain Concepts**](bitcoinRefMaterials.md)
[**Math**](mathRefMaterials.md)
[**Ethereum**](ethereumRefMaterials.md)

## In Class Notes and Recordings


[Schnorr signatures](/notes/schnorr2022.pdf)
[Diffie Hellman Key Exchange](/notes/diffie_hellman_on_groups.pdf)

[**midterm topics**](/notes/midterm_topics_2022.pdf)


## Weekly Process

Each week we'll follow the same pattern:

1. Skim the PDF materials.
2. Watch the video, which is an overview of the PDFs. It does not replace the PDFs!
3. Read through the PDF in detail
4. Come to class prepared for discussion with questions remaining from what you read, and ready to answer questions from your instructor. Discussion is not a repeat of the videos.  It's a discussion.  Attendance is mandatory!

# Week 12: (Nov 30 - Dec 6) Lightning And Bitcoin Layer 2

- [Lightning Video Lecture](/week/lightning/lightning.mp4)
- [Taproot description](https://bitcoinmagazine.com/technical/taproot-coming-what-it-and-how-it-will-benefit-bitcoin)


### Taproot/Lightning metrics

- [lightning](https://bitcoinvisuals.com/lightning)

- [Percent of taproot spends](https://transactionfee.info/charts/transactions-spending-taproot/)
- [Taproot script path spends](https://transactionfee.info/charts/inputs-p2tr-scriptpath/)
- [Taproot key path spends](https://transactionfee.info/charts/inputs-p2tr-keypath/)

# Week 11: (Nov 16 - Nov 29) Satoshi (Bitcoin) Scripting

- Bitcoin Virtual Machine Description: https://github.com/bitcoinbook/bitcoinbook/blob/develop/ch06.asciidoc#transaction-scripts-and-script-language
- [Bitcoin (actually Nexa) instruction set (opcode) Reference](https://spec.nexa.org/protocol/blockchain/script#operation-codes-opcodes).  Yes, actually READ through these!  (so you get a sense of what can and can't be done)
- [Bitcoin Scripts and Design Patterns](/week/transactions/example_scripts.pdf)

### Tools
 - Nexa script debugger: https://debug.nexa.org/
 - Bitcoin Scripting online: https://siminchen.github.io/bitcoinIDE/build/editor.html


# Week 10: (Nov 9 - Nov 15) Byzantine Generals Problem
[Byzantine Generals Video Lecture](/week/byzantine_generals/Byzantine_Generals.mp4)

[Vukolic paper: Proof-of-work vs. BFT Replication](/papers/iNetSec_2015.pdf)
[Byzantine Fault Tolerance vs Proof Of Work Video Lecture](/week/byzantine_generals/BFT_POW_Comparison.mp4)

### Reference

[Byzantine Generals Video Lecture Notes](/week/byzantine_generals/Byzantine_Generals.pdf)
[FLP, CAP, BFT Comparison lecture notes](/week/byzantine_generals/BFT_POW_Comparison.pdf)

[(optional) Impossibility Of Distributed Consensus (original FLP paper)](/papers/Impossibility_Distributed_Consensus_FLP_jacm85.pdf)

# Week 9: (Nov 2 - Nov 8) Blockchain Applications: Voting
[Blockchain-Based E-Voting System](/papers/Research-Paper-BBEVS.pdf)
Focus on the requirements of voting.  Much of the details of this voting system is Ethereum specific and so the prereqs to understanding it haven't been covered in class.  
[Voting's impossible triangle](https://www.bitcoinunlimited.net/voting_impossible_triangle.md)
[Blockchain Voting Techniques](https://www.bitcoinunlimited.net/blockchain_voting.md)

# Week 8: (Oct 25 - Nov 1) Blockchain Applications: Tokens

- [Native Blockchain Tokens (Group Tokenization Proposal)](https://docs.google.com/document/d/1X-yrqBJNj6oGPku49krZqTMGNNEWnUJBRFjX7fJXvTs/edit?usp=sharing)
	- Miner validated tokens on bitcoin-like blockchains.  Skip the intro, read "Functional Description", up to "Contract Encumbered Groups".  Read the use cases to understand general token use cases for any token technology on any blockchain.
- Ethereum Tokens
	- Contract-implemented and enforced tokens (similar to what you did in cryptozombies).  Follow the link into the EIPs to see the Solidity interface.
	 - [ERC-20 Token Standard](https://ethereum.org/en/developers/docs/standards/tokens/erc-20/)
	 - [ERC-721 Non-fungible Tokens](https://ethereum.org/en/developers/docs/standards/tokens/erc-721/)

# Week 7: Midterm

- Please complete cryptozombies lessons 4-6 (https://cryptozombies.io/en/course)  

# Week 6: (Oct 5 - 11) Block Propagation (Bloom filters, Graphene)

[Efficient Block Propagation Lecture Video](/week/block_propagation/graphene_lecture.mp4)

[Bloom Filter Lecture Notes](/week/block_propagation/bloom_filters_notes.pdf)
[Invertible Bloom Lookup Table (IBLT)](/papers/IBLT_1101.2245.pdf)
[(661) Graphene Paper](/papers/graphene.sigcomm.pdf)
[(461) Graphene short paper](/week/block_propagation/graphene_short.pdf)

- [Cryptozombies Ethereum (Solidity) programming tutorial (courses 1, 2, and 3 only)](https://cryptozombies.io/en/course)


### Reference
[Bloom Filter original paper](/papers/bloom_filters_10.1.1.20.2080.pdf)
[Graphene Lecture at Scaling Bitcoin 2017](https://youtu.be/BPNs9EVxWrA)

## Week 5: (Sep 28 - Oct 4) Bitcoin Networking, Eclipse Attacks, and Distributed Clock Synchronization

- [Networking and Eclipse Attack Video Lecture](/week/networking_eclipse/Networking_Eclipse.mp4)
- [Single transaction block analysis](/papers/1txn.pdf)

- **Clock Synchronization**

We are going to use short excerpts from "Distributed Systems: Principles and Paradigms" by Andrew S. Tanenbaum and Maarten Van Steen. Fortunately, the entire text is available for free! Just give any email address to the [author's web site](https://www.distributed-systems.net/index.php/books/ds3/) and they'll send you a PDF.

Please read:
-   Section 6.1: Clock Synchronization (no need to read the Berkeley algorithm or about wireless networks)
-   Section 6.2: Logical clocks

Later in the semester, we'll use the text for another topic.

- [Logical Clock Conditions - Georgia Tech](https://www.youtube.com/watch?v=gaptoWIYgeg)
- [Real World Scenario (clocks) - Georgia Tech](https://www.youtube.com/watch?v=HX_EA8_nG54)

### Reference
 - [**Eclipse Attacks on Bitcoin's Peer-to-Peer Network**](/papers/sec15-paper-heilman.pdf)

Purely optional reading for the course, but if you are interested, here is the original Lamport paper:
-   [**Time, clocks, and the ordering of events in a distributed system**](http://amturing.acm.org/p558-lamport.pdf), by Leslie Lamport, Communications of the ACM; 21(7): 558–565. (1978)

#### Homework
 * Build your own blockchain

## Week 4: (Sep 21 - 27) Elliptic Curve Crypto

- [ECC Video Lecture part 1](/week/elliptic_curve_crypto/ECC_part1.mp4)
- [ECC Video Lecture part 2](/week/elliptic_curve_crypto/ECC_part2.mp4)

### Reference
[(Optional) elliptic-curve-cryptography-a-gentle-introduction](https://andrea.corbellini.name/2015/05/17/elliptic-curve-cryptography-a-gentle-introduction/)
[(Optional) ECC part 2](https://andrea.corbellini.name/2015/05/23/elliptic-curve-cryptography-finite-fields-and-discrete-logarithms/)
[(Optional) Chapter 8, Paar and Pelzl](/papers/paar_pelzl_chapter_8_excerpt.pdf)
[(Optional) Chapter 9, Paar and Pelzl](/papers/paar_pelzl_chapter_9.pdf)

## Week 3: (Sep 13 - 20) Selfish Mining, Sybil Attack

- [Selfish Mining paper](/papers/selfish_mining-btcProcFC.pdf )
- [Sybil Attack paper](/papers/sybil_attack-IPTPS2002.pdf)

- [Sybil Attack Lecture](/week/sybil/Sybil_Attacks.mp4)
- [Selfish Mining Lecture](/week/selfish/Selfish_Mining.mp4)

- Skim this paper [Attacks against Autofinalization and Parking](/papers/autofinalization_parking.pdf).
Understand:
  * That a variety of "tweaks" can be invented to modify Satoshi's algorithm to choose the "main chain"
  * What two proposed tweaks: "autofinalization" and "parking" actually do
  * The hybrid computational/mathematical technique used to generate the graphs.  Examine [the python code](/papers/autofinalization_parking_py.md) as your guide.
  * There seems to be no description of this consensus change, except for the [source code located in this function](https://github.com/Bitcoin-ABC/bitcoin-abc/blob/7e8aab945660cbb148bea1db029300568c543e68/src/validation.cpp#L2567) (reading just the comments may be helpful).


#### Homework 3
Questions
Python Markov Process summation

### Reference
- [Sybil Attack Lecture Notes](/week/sybil/Sybil_Attacks.pdf)
- [Selfish Mining Lecture Notes](/week/selfish/Selfish_Mining.pdf)

## Week 2: (Sep 6-13) Cryptography Overview, Doublespend Attacks 

We continue our investigation into basic blockchain architecture, and at the same time jump to the highest levels and start learning how to write contracts in Ethereum.   
  
- [Overview of Applied Cryptography Video Lecture](/week/crypto_overview/cryptography_intro.mp4)
-  [Doublespend Attack Video Lecture (1/2)](/week/doublespend_attack/doublespend_intro.mp4)
-  [Doublespend Attack  (2/2)](/week/doublespend_attack/doublespend_analysis.mp4) -- note the final equation in this video has some typos although the derivation is correct in essence.  Use the equation in Satoshi's white paper to calculate the likelihood of matching the main chain.  And to calculate the 
- probability of exceeding the main chain:

$$\lambda = \frac{(z+1)q}{p}$$
$$1- \sum_{k=0}^{z+1} (\frac{\lambda^ke^{-\lambda}}{k!})(1-(q/p)^{z+1-k}), if q < p$$

#### Homework 2
Questions and Python Satoshi & MonteCarlo doublespend *


### Reference
- [Overview of Applied Cryptography Notes](/week/crypto_overview/overview_of_applied_cryptography.pdf) 
- [Doublespend Analysis Notes](/week/doublespend_attack/doublespend_analysis.pdf) -- with correct doublespend derivation 
- [basic probability](/notes/simple_probability.pdf)

## Week 1: Introduction
These are the materials for our first week.

- [The Bitcoin Whitepaper](/papers/Bitcoin-satoshi.pdf)

- [Introduction  and Class Logistics Video Lecture](/week/introduction/intro-definitions.mp4)

- [Blockchains Part 1 Video Lecture](/week/introduction/blockchains_part_1.mp4)
- [Blockchains Part 2 Video Lecture](/week/introduction/blockchains_part_2.mp4)

#### Homework 1
Questions

### Reference

- [Introduction  and Class Logistics Video Notes](/week/introduction/introduction_and_class_logistics.pdf)
- [Blockchains Video Notes](/week/introduction/Bitcoin_and_Nakamotos_paper.pdf)

- [blockchain in one diagram](/notes/blockchain_in_one_diagram.pdf)



# EXTRA
## Ethereum Blockchain
- [Ethereum video lecture](/week/ethereum/Ethereum_Intro.mp4)
- [Proof of Stake (Casper)](/week/ethereum/casper_proof_of_stake.mp4)
- [DeFi and Dapps](/week/ethereum/dapps.mp4)

### Reference
- [ethereum video lecture notes](/week/ethereum/Ethereum_Intro.pdf)
