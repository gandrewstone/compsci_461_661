I've introduced you to a friend who is an alien from the planet Betelgeuse-9. They don't use numbers like we do! They use an ordered system of symbols as follows: dot $\bullet$, square $\blacksquare$,  chi $\chi$, triangle $\blacktriangle$, circle $\circ$, clubs $\clubsuit$, diamond $\diamondsuit$, dash $-$, and heart~$\heartsuit$.  This system also has an operator ``+".  

I tell the alien that as a shorthand, we can also define a scalar ``multiply" operation n*S  (where n is a scalar, and S is a symbol in the group) which just means S+S+...+S (n times).

$\blacksquare \oplus \blacksquare = 2 \otimes \blacksquare = \chi$
$\blacksquare \oplus \blacksquare \oplus \blacksquare = 3 \otimes \blacksquare = \blacktriangle$
$\blacksquare \oplus \blacksquare \oplus \blacksquare \oplus \blacksquare  = 4 \otimes \blacksquare = \circ$
and so on. 

Also note that $\bullet \oplus \blacksquare = \blacksquare$, and $\heartsuit \oplus \blacksquare = \bullet$

This is an Abelian (i.e., commutative) group.  Don't be confused by these alien symbols!  Remember that a group follows simple rules, and the algorithms we discussed in class apply to any commutative group.  

**Hint, perhaps finish the table of how many squares sum to each group element to make multiplication simple.**

*What special role does the square symbol play in defining this  group?  How about the dot?*

<br/>
<br/>

*Demonstrate to the alien the Diffie Helman algorithm between two participants, Alice and Bob, using this alien's group.  Use 3 for Alice's private key, and 5 for Bob's private key.*

<br/>
<br/>

*Demonstrate a Schnorr signature using this group, assuming the private key is 4, and the hash of the message is 2.*

<br/>
<br/>

*Why isn't this system secure?*

<br/>  
<br/>