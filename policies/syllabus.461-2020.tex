%\documentclass[onecolumn,12pt]{article}
%\usepackage[margin=2in,bmargin=.3in,tmargin=.3in]{geometry}
\documentclass[twocolumn,10pt]{article}
\usepackage[margin=.8in,bmargin=1in,tmargin=1in]{geometry}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{ifpdf}
\usepackage{microtype,url,relsize,cite}


\pagestyle{fancy}
\lhead{COMPSCI 461}
\chead{}
\rhead{Fall 2020}
\lfoot{}
\cfoot{\thepage \ of \pageref{LastPage}}
\rfoot{}

% \ifpdf
% \usepackage[pdftex]{hyperref}
% \else
% \usepackage[ps2pdf]{hyperref}
% \fi

\title{COMPSCI 461:\\Secure Distributed Systems\\ {\sc syllabus}}
\author{Prof.\ Andrew Stone}
\date{Last revised: \today}
\begin{document}

\newcommand{\para }[1]{\smallskip \noindent {\bf #1}}

\maketitle

\thispagestyle{fancy}

\section{Important Details}
\para{Credits:} 3

\para{Teaching Assistant:}\\ Sandeep Polisetty (spolisetty@cs.umass.edu) \\
\para{Undergraduate Class Assistant:}\\ Sanchit Nevgi (snevgi@umass.edu)

\para{Piazza:} {\\\relsize{-.5}\url{https://piazza.com/class/kdt6eojhdqi5c5}} \\
\para{Moodle (course materials):} {\\\relsize{-.5}\url{https://moodle.umass.edu/course/view.php?id=71108}} \\


\para{When/Where:}  ONLINE

\para{Readings:} There is no textbook for this class. Instead, we will be reading several prepared notes authored by Brian Levine and myself and various supplementary readings.

\para{Office Hours:} Tuesdays 10am-11am and by appointment. 
Held online.  Contact me via piazza to set up a time, even during normal office hour time, so I can schedule students. \\
Office hours will also be offered by the teaching assistants, times and places TBA.

\para{Prereqs:}  (CMPSCI 220 or CMPSCI 230) and CMPSCI 240. 


\section{Introduction}
This class is devoted to the study of securing distributed systems. Our goal is to explore a broad collection of classic topics in security, network, and distributed systems. Blockchains will serve as a common thread and  cohesive structure for us, however, many topics that we will cover are  applicable well beyond  blockchains and similar distributed, open ledgers. A blockchain  is a novel distributed system that (probabilistically) provides secure computation and storage by carefully orchestrating  economic incentives among a set of untrusted peers. Unlike other distributed systems, economic value is an integral component of  blockchains.

We'll start with  fundamental results from Lamport, Fischer, and Douceur that fence-in consensus systems, including blockchains. We'll also look at the efficiency of the network architectures for peer-to-peer communication and attacks on their security (e.g., eclipse/denial-of-service attacks). And we'll review the operation of applied crypto including hash functions and elliptical curves (used to validate transactions). Other topics include secure network protocol design, secure distributed programming (via Solidity), probabilistic data structures, privacy,  economics, and finance. In examining these concepts, we'll make use of notions from basic probability, Monte Carlo simulation methods, and some proofs on the correctness of algorithms. 

%
%In many ways, our goal is to explore this broad collection of topics in security, network, and distributed systems with blockchains being the common thread that allows a cohesive structure. You'll learn a lot in this class that is applicable well beyond bitcoin and blockchains.

%Evaluating and addressing the security and privacy needs of real systems is increasingly a multidisciplinary task. Even if systems could be ``fully secured" against all technical vulnerabilities they would still play a role in violating higher-layer policies. For example, most systems can be used to aid and abet crimes that harm persons, steal resources, or play a role in creating risk. Multidisciplinary work has a greater chance at addressing problems beyond technical flaws. In this course, I will use blockchain solutions as a platform for studying these issues. I will examine the cryptography that supports blockchains, security and privacy attacks on its Internet-based architecture, and the financial, legal, and social issues that the technology perturbs. 

Assignments will include advanced programming projects and reading research papers.  Students will complete these readings, several take-home assignments based on the readings, and participate in a lively class discussion. In addition, there will be a midterm and final exam. Students will be asked to express an opinion on many topics and challenge the instructor's views and analyses.

The specific objectives for the course are as follows:
\begin{itemize}
\item To gain a deep understanding of secure distributed systems, with attention paid to  underlying theory as well as the practical blockchain technologies that are in wide use today.
\item To gain an understanding of the broader implications of this technology in terms of  finance and economics. 
\item To gain experience in making well-reasoned arguments during class discussion.
\end{itemize}

Because of the programming assignments, students will need prior experience programming. You must use Python for these assignments. We'll also write Ethereum software contracts in a language called Solidity that I don't expect you to have used before (it's similar to java/python/C), and Bitcoin Script which is similar to a stack-only assembly language.

\subsection{Flipped Classes}

This course will be making use of a ``flipped classroom" model. Lectures will be pre-recorded and available online. We meet once a week for discussion. Discussions will be carried out assuming that students have not only completed readings and assignments, but that the pre-recorded lectures have been viewed. There will be some work assigned and completed during discussions (included in ``written assignments" portion of the grade). Students that do not attend discussion will lose points towards their final grade.

%The class will be offered somewhat concurrently as cs490P and cs690P. The pre-recorded lectures will be the same. The discussion sections for 490P will focus on practical topics, whereas the 690P sections will focus on additional details and some additional research papers. The assignments will be shared at their core, but the tasks may be slightly different. 

\subsection{List of Topics}
Below are an overview of topics covered in this course. The moodle site has more specifics and last minute changes.

\begin{enumerate}
%%%%%
\item Applied cryptography
\begin{itemize}
\item definition of security
\item hash functions
\item Merkle trees
\item public/private key crypto using elliptic curves
\end{itemize}
%%%%%
\item Blockchains
\begin{itemize}
\item Nakamoto consensus 
\item Details of Bitcoin: transactions, blocks, p2p networking
\item Ethics
\item Doublespend attacks (including Gambler's Ruin)
\item Selfish mining attacks
\item Eclipse attacks
\end{itemize}
%%%%%
\item Distributed Systems
\begin{itemize}
\item Doucer's Sybil attack impossibility result
\item Clocks: NTP and Lamport clocks
\item Lamport's byzantine general's result
\item Fischer, Lynch, and Paterson's (FLP) impossibility result
\end{itemize}
%%%%%
\item More Bitcoin Details
\begin{itemize}
\item Bitcoin's scripting language
\item Difficulty adjustment algorithms and hashrate estimation
%\item Transaction malleability
\item Lightning networks
%\item Segwit
 \end{itemize}
%%%%%
\item Ethereum
\begin{itemize}
\item Ghost
\item ETHASH
\item OP codes
\item Patricia Merkle Trees
\item DAPPS
\item Programming Ethereum with Solidity
\end{itemize}
%%%%%
\item Finance
\begin{itemize}
\item basic overview of economic metrics
\item basic overview of financial instruments (e.g., futures)
\item Initial Coin Offerings
%\item Using futures contracts to insure DAPPs
\end{itemize}
%%%%%
\item Improving Blockchain performance
\begin{itemize}
\item Bloom filters
\item Invertible Bloom Lookup Tables (IBLTs)
\item Compact Blocks
\item Graphene
%\item Low variance mining with Bobtail
\item Proof of stake based blockchains
%\item DAG-based blockchains
\end{itemize}
\end{enumerate}

\section{Inclusive Discussion}
In this course, each voice in the classroom has something of value to contribute. Please take care to respect the different experiences, beliefs and values expressed by students and staff involved in this course. I support the  commitment of the UMass Amherst College of Information and Computer Sciences to diversity, and welcome individuals of all ages, backgrounds, citizenships, disability, sex, education, ethnicities, family statuses, genders, gender identities, geographical locations, languages, military experience, political views, races, religions, sexual orientations, socioeconomic statuses, and work experiences.

 

\section{Grading}
Your overall grade for the course will be derived from three components. At a high-level grading is based on the following formula:
\begin{itemize}
\item 50\% Written Assignments (including assignments completed during discussion)
\item 20\% Midterm Exam (evening exam, date TBA) %7pm--9pm during the week of March 5) 
\item 20\% Final Exam (during finals week)
\item 10\% Class participation (including attendance in discussion and online participation) 
\end{itemize}
Additionally, without a  grade of 50\% or higher on each of the two exams, students cannot pass the class. 

Each assignment will have a slightly different number of points. Your score will be the total number of points earned over total number of points available for all assignments. Late homeworks are NOT accepted. 

%Final letter grades will be based on the class curve. I typically pick a B to be the midpoint of the curve but I reserve the right to pick that midpoint at the end of the semester based on class performance. Because I use a curve, that means that if the entire class does very, very well and you just do well, you might receive a C grade or lower. The range of each letter will be based on the standard deviation of the class grades.  

Grading Scale: \\
\begin{tabular}{ll}
A  & 96\% and above     \\               
A$-$ & [93--96\%)    \\
B+ & [90--93\%)   \\
B  & [87--90\%)  \\                     
B$-$ & [84--87\%)  \\ 
C+ &[81--84\%)    \\                 
C  & [78--81\%)\\
C$-$ & [75--78\%)\\
D  & [72--75\%)       \\
F  & below 72\%      \\             
 \end{tabular}
 
A curve may be applied to individual assignments or the final grade calculation. 
 
Don't underestimate the {\em Class Participation} component --- full credit versus none can  move your final grade by quite a bit.

\subsection{Homeworks}

I will use web interfaces exclusively to accept assignments, which must be in the form of a PDF (no word, text, or other formats), with your name clearly visible. In the case that an assignment involves code, please submit a tar-ball or zip file.  I will not accept assignments late, and I will assign a score of zero for work that is not submitted on time (or at all). 

If class participation is generally low, or if I get the sense that students aren't reading, or if it seems like good preparation for the midterm or final exams, I will give in-class quizzes. These quizzes may not be pre-announced. They will become part of the homework component of your grade.

\textbf{Assignments that do not compile will receive no credit.}


\subsection{Exams}

There will be a midterm exam and a final exam.  The midterm will be given at 3/7/19 from 7-9PM in ILC S131.  The final is not cumulative.  It will cover material presented after the midterm, though some references to pre-midterm material are inevitable and are to be expected.  



\subsection{Class Participation}

I will assign this portion of your grade on the basis of your presence and participation in class.  Obviously, I expect you to always attend class.  Further, I expect you to participate in class discussion, posing and answering questions as appropriate. Also, I expect that you'll leave room for others to speak their minds as well.   I will provide feedback  about halfway through the semester as to the status of this portion of your grade.  

If I have outside experts join us in class, not attending on these days will weigh more heavily against your participation grade.  

One optional way to improve your class participation grade is to offer up to the entire class your {\em scribe} notes from discussion or the online videos. I'll provide a google doc for each discussion meeting and video to add your comments to.


\section{Policies}
 All official material for the class can be found on the UMass Blackboard site. Any external course web page is more of an advertisement for the class and won't be kept up to date.

%
%All assignments must be handed in through the Blackboard interface. See the note below about our use of {\em Turn It In}. 

Cell phones, laptops, and similar devices may not be used during class. 




\subsection{Collaboration and Plagiarism}
Please come see me if you are unable to keep up with the work for this class, for any reason, and I will work something out. Obviously, there isn't anything I can do when the semester has already ended. I want to see you succeed and will do everything I can to help you out. The earlier you let me help, the more help I can offer.
 

Please be cognizant of the University's policies on cheating. You may discuss material with others, but your writing must be your own. When in doubt, contact me about whether a potential action would be considered plagiarism. When discussing problems with others, do not show any of your written solutions. When asking others for help, do not take notes about the solution other than to jot down publicly available references. Use only verbal communication.

If you do discuss material with anyone besides the instructors, acknowledge your collaborators in each write-up. If you obtain a key insight with help (e.g., through library work or a friend), acknowledge your source, briefly state the insight, and write up the solution on your own. I expect to see citations if you use an outside source (other than the assigned articles) to complete an assignment.  You may directly quote from a decision in order to complete a brief --- provided you surround the text by quotation marks --- without citation. 

It is never permissiable to distribute your completed assignments, my homework solutions, exams, or exam solutions to other persons nor to post these materials to Internet sites, including Github and Course Hero. Of course it is not permissible to use such resources as well to look at course or prior course materials. Both are obvious violations of the University's academic honesty policies and I will pursue sanctions even after the course is over.

Never misrepresent someone's work as your own. It must be absolutely clear what material is your original work. You must remove any possibility of someone else's work from being misconstrued as yours.

As a condition of continued enrollment in this course, you agree to submit all assignments to the Turnitin and/or My Drop Box services for textual comparison or originality review for the detection of possible plagiarism. All submitted assignments will be included in the UMass Amherst dedicated databases of assignments at Turnitin and/or My Drop Box. These databases of assignments will be used solely for the purpose of detecting possible plagiarism during the grading process and during this term and in the future. Students who do not submit their papers electronically to the selected service will be required to submit copies of the cover page and first cited page of each source listed in the bibliography with the final paper in order to receive a grade on the assignment.

You can and should read the University's policies on cheating as well at \url{http://www.umass.edu/ombuds/honesty.php/}. In short, intellectual honesty requires that students demonstrate their own learning during examinations and other academic exercises, and that other sources of information or knowledge be appropriately credited. Scholarship depends upon the reliability of information and reference in the work of others. Student work at the University may be analyzed for originality of content. Such analysis may be done electronically or by other means. Student work may also be included in a database for the purpose of checking for possible plagiarized content in future student submissions. No form of cheating, plagiarism, fabrication, or facilitating dishonesty will be condoned in the University community. (Some portions of the above plagiarized from \url{http://www.umass.edu/academichonesty/AddressingPlagiarism.html}!)

\section{UMass Policies}
\para{Accommodation Statement.}
The University of Massachusetts Amherst is committed to providing an equal educational opportunity for all students.  If you have a documented physical, psychological, or learning disability on file with Disability Services (DS), you may be eligible for reasonable academic accommodations to help you succeed in this course.  If you have a documented disability that requires an accommodation, please notify me within the first two weeks of the semester so that we may make appropriate arrangements.

\para{Academic Honesty Statement.}
Since the integrity of the academic enterprise of any institution of higher education requires honesty in scholarship and research, academic honesty is required of all students at the University of Massachusetts Amherst.  Academic dishonesty is prohibited in all programs of the University.  Academic dishonesty includes but is not limited to: cheating, fabrication, plagiarism, and facilitating dishonesty.  Appropriate sanctions may be imposed on any student who has committed an act of academic dishonesty.  Instructors should take reasonable steps to address academic misconduct.  Any person who has reason to believe that a student has committed academic dishonesty should bring such information to the attention of the appropriate course instructor as soon as possible.  Instances of academic dishonesty not related to a specific course should be brought to the attention of the appropriate department Head or Chair.  Since students are expected to be familiar with this policy and the commonly accepted standards of academic integrity, ignorance of such standards is not normally sufficient evidence of lack of intent (\url{http://www.umass.edu/dean_students/codeofconduct/acadhonesty/}).


\end{document}
