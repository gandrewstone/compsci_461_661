#!/usr/bin/python3
# Blockchain reorganization probability calculator
# Copyright 2020 G. Andrew Stone
# MIT Licensed (https://opensource.org/licenses/MIT)

# This file calculates and plots:
# 1. Satoshi (tie) double spend attack (chain reorganization) probabilities
# 2. Winning (1 extra block) double spend attack probabilities
# 3. Limited depth double spend attack probabilities
# 4. Chain matching attack probabilities verses finalization and/or parking blockchains

# dependencies:
# sudo apt-get install python3-matplotlib

import math

def poisson(k, lam):
    """
    Poisson probability mass function.  See https://en.wikipedia.org/wiki/Poisson_distribution
    k is the number of occurrences (# of successful blocks found in our case)
    lam (lambda) is the expected value of the poisson random variable
    """
    return (  (lam**k)/(math.factorial(k)*(math.e**lam)) )

def poissonNabove(k, lam):
    """
    The likelihood that k or greater occurrences happen
    """
    acc = 0.0
    for i in range(0,k):  # goes from 0 to k-1 inclusive
        acc += poisson(i,lam)
    return 1-acc

def dspart(z,q,k):
    """
    The part of the doublespend attack equation inside the summation.  See the bitcoin whitepaper
    """
    p = 1.0 - q
    lam = z*q/p
    a = (lam**k)/(math.factorial(k)*(math.e**lam))
    b = 1.0 - ((q/p)**(z+1-k))
    return a*b

def doublespendAttack(z,q):
    """
    Returns the probability of a doublespend attacker that applies q hash power (as a fraction of the total hash)
    will exceed the main chain within z blocks
    """
    if (q>0.5):
        return 1.0
    sm = 0.0
    for k in range(0,z+2): # +2 because range is not end inclusive, and it must be +1 because the attacker chain must exceed the honest
        t = dspart(z,q,k)
        sm += t
    return 1.0 - sm

def doublespendAttackTie(z,q):
    """
    Returns the probability of a doublespend attacker that applies q hash power (as a fraction of the total hash)
    will tie the main chain within z blocks
    """
    if (q>0.5):
        return 1.0
    sm = 0.0
    for k in range(0,z+1): # +1 because range is not end inclusive
        t = dspart(z,q,k)
        sm += t
    return 1.0 - sm



calced2={}
def FinParkFork(attackerHash, Mlen, Flen, finalizationDepth, park):
    """
    Returns the probability that an attacker will keep a viable fork going for finalizationDepth blocks.
    attackerHash is the fraction of hash power the attacker has (0-1)
    Mlen and Flen are the starting points of the 2 chains (pass 0,0 for typical scenarios) (Mlen is the main chain, Flen is attacker fork)
    park: True to model the "parking" algorithm described in the paper, False to model finalization only
    """

    # If the fork has persisted until the finalization depth has been hit then the attacker wins.
    # As the main chain finds blocks, the attacker is ready to inject their blocks, keeping the fork viable.
    if (Flen >= finalizationDepth):
        return 1.0

    if park:
        # parking is disabled in fork depth 1 & 2 -- attacker must have found the block first
        if (Mlen<3 and Flen<Mlen):
            return 0.0
        # can fall behind no more than 1 block in fork depth 3
        elif (Mlen==3 and Flen<2):
            return 0.0
        # for other fork depths, attacker cannot fall behind more than double
        elif (Flen*2 < Mlen):
            return 0.0
    elif (Flen < Mlen):  # without parking, fork cannot ever fall behind
        return 0.0

    # note that the attacker (Flen) can get arbitrarily far AHEAD of the main chain because in that case the attacker
    # withholds their blocks until the moment the main chain finds theirs.

    # Look in the cache for values we've already found
    if (float(attackerHash),Mlen,Flen,finalizationDepth,park) in calced2:
        return calced2[(float(attackerHash),Mlen,Flen,finalizationDepth,park)]

    # ok not found, we need to calculate it
    honestHash = 1-attackerHash
    interval = attackerHash/honestHash

    # calculation proceeds as follows:
    # the total probability is the sum of the probabilities of all the different possibilities.
    # The possibilities are:
    #    (1) Over the interval where the main chain finds 1 block, the fork finding M -> [0, N) blocks (where anything N and above is a win)
    #        (2) This is the probability of finding M blocks * the probability of a win from that state (recursive)
    #    (3) The probability the attacker finds N or more blocks

    acc = 0.0
    for i in range(0,finalizationDepth-Flen):  # (1)
        # Model the F chain finding i blocks while the M chain finds 1
        acc += poisson(i,interval)*FinParkFork(attackerHash, Mlen+1, Flen+i, finalizationDepth, park)  # (2)
    # Model the F chain finding more than what it needs to win in this one interval
    acc += poissonNabove(finalizationDepth-Flen, interval) # (3)

    calced2[(float(attackerHash),Mlen,Flen,finalizationDepth,park)] = acc
    return acc

calced3={}
def limitedDoubleSpendAttack(attackerHash, embargo, maxDepth, Mlen, Flen, park):
    if park is True: # we are assuming the embargo is > 3 so the early parking rules do not matter
        if Flen > Mlen*2 and Flen > embargo:
            return 1.0
        calcDepth = maxDepth*2
    else:
        if Flen > Mlen and Flen > embargo:
            return 1.0
        calcDepth = maxDepth

    if (Mlen >= maxDepth): # Too late!
        return 0.0

    # Look in the cache for values we've already found
    if (float(attackerHash),embargo,Mlen,Flen,maxDepth,park) in calced3:
        return calced3[(float(attackerHash),embargo,Mlen,Flen,maxDepth,park)]

    honestHash = 1-attackerHash
    interval = attackerHash/honestHash

    acc = 0.0
    for i in range(0,calcDepth-Flen):
        # Model the F chain finding i blocks while the M chain finds 1
        acc += poisson(i,interval)*limitedDoubleSpendAttack(attackerHash, embargo, maxDepth, Mlen+1, Flen+i, park)
    # Model the F chain finding more than what it needs to win in this one interval
    acc += poissonNabove(calcDepth-Flen, interval)

    calced3[(float(attackerHash),embargo,Mlen,Flen,maxDepth,park)] = acc
    return acc


import numpy as np
import matplotlib.pyplot as pyplot

def plotFig1():
    x = np.linspace(start=0.0, stop=0.99, num=100)
    fig = pyplot.figure(1)
    plt = fig.add_subplot()
    plt.grid(color=(0.8,0.8,0.8))
    plt.set_xlabel('Attacker hash proportion', fontsize=15)
    plt.set_ylabel('Success probability', fontsize=15)

    y2 = [ FinParkFork(i,0,0,1,False) for i in x]
    plt.plot(x, y2, color=(.97,.95,0.9))

    y2 = [ FinParkFork(i,0,0,2,False) for i in x]
    plt.plot(x, y2, color=(.95,.88,0.8))

    y1 = [ FinParkFork(i,0,0,5,False) for i in x]
    plt.plot(x, y1, color=(.9,.8,.7))


    y3 = [ FinParkFork(i,0,0,20,False) for i in x]
    plt.plot(x, y3, color=(.8,.7,.8))
    y4 = [ FinParkFork(i,0,0,40,False) for i in x]
    plt.plot(x, y4, color=(.9,.8,.9))

    y = [ FinParkFork(i,0,0,10,False) for i in x]
    plt.plot(x, y, color=(.9,0,0))

    fig.suptitle("Success Probability for the Matching Fork Attack\nat 1,2,5,10(red),20,and 40 block autofinalizations", fontsize=16)
    fig.show()

def plotFig2():
    x = np.linspace(start=0.0, stop=0.99, num=100)
    fig = pyplot.figure(2)
    plt = fig.add_subplot()
    plt.grid(color=(0.8,0.8,0.8))
    plt.set_xlabel('Attacker hash proportion', fontsize=15)
    plt.set_ylabel('Success probability', fontsize=15)

    y2 = [ FinParkFork(i,0,0,1,True) for i in x]
    plt.plot(x, y2, color=(.97,.95,0.9))

    y2 = [ FinParkFork(i,0,0,2,True) for i in x]
    plt.plot(x, y2, color=(.95,.88,0.8))

    y1 = [ FinParkFork(i,0,0,5,True) for i in x]
    plt.plot(x, y1, color=(.9,.8,.7))

    y3 = [ FinParkFork(i,0,0,20,True) for i in x]
    plt.plot(x, y3, color=(.8,.7,.8))
    y4 = [ FinParkFork(i,0,0,40,True) for i in x]
    plt.plot(x, y4, color=(.9,.8,.9))

    y = [ FinParkFork(i,0,0,10,True) for i in x]
    plt.plot(x, y, color=(.9,0,0))

    fig.suptitle("Success Probability for the Matching Fork Attack\nat 1,2,5,10(red),20,and 40 block parked autofinalizations", fontsize=16)
    fig.show()

def plotFig3_differentAttacks():
    x = np.linspace(start=0.0, stop=0.99, num=100)
    fig = pyplot.figure(3)
    plt = fig.add_subplot()
    plt.grid(color=(0.8,0.8,0.8))
    plt.set_xlabel('Attacker hash proportion', fontsize=15)
    plt.set_ylabel('Success probability', fontsize=15)

    # Green: standard doublespend attack, with a 10 block embargo period
    z = [ doublespendAttack(10,i) for i in x]
    plt.plot(x, z, color=(0,.9,0))

    # Blue: 10 block doublespend attack with 10 block embargo period
    embargo = 10
    ldsa = [ limitedDoubleSpendAttack(i, embargo, 10, 0, 0, False) for i in x]
    plt.plot(x, ldsa, color=(0,0,.9))

    # Yellow: 10 block DS with 10 block embargo & parking
    ldsa = [ limitedDoubleSpendAttack(i, embargo, 10, 0, 0, True) for i in x]
    plt.plot(x, ldsa, color=(0.9,0.9,0))

    # Red: 10 block Fork Matching attack against finalization & parking
    y = [ FinParkFork(i,0,0,10,True) for i in x]
    plt.plot(x, y, color=(.9,0,0))

    # Orange: 10 block Fork Matching attack against finalization only
    yy = [ FinParkFork(i,0,0,10, False) for i in x]
    plt.plot(x, yy, color=(.9,.5,.2))

    fig.suptitle("Attack Comparison", fontsize=16)
    fig.show()

def Test():
    print("F  50% at 10: ", FinParkFork(0.5,0,0,10,False))
    print("F  66% at 10: ", FinParkFork(2.0/3.0,0,0,10,False))
    print("F  75% at 10: ", FinParkFork(0.75,0,0,10,False))
    print("PF 50% at 10: ", FinParkFork(0.5,0,0,10,True))
    print("PF 66% at 10: ", FinParkFork(2.0/3.0,0,0,10,True))
    print("PF 75% at 10: ", FinParkFork(0.75,0,0,10,True))

    plotFig1()
    plotFig2()
    plotFig3_differentAttacks()

if __name__ == "__main__":
    Test()
    import code
    code.interact()
