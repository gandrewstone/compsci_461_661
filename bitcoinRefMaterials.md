# Bitcoin Reference Materials

****["Mastering Bitcoin" overview book](https://github.com/bitcoinbook/bitcoinbook)****

[bitcoin.org](https://bitcoin.org/en/developer-reference)

## Bitcoin Cash Reference Materials
[reference.cash -- protocol, instruction set, etc](https://reference.cash)


## Light Wallets ##

**[HD wallet key derivation](/notes/HD_wallet_key_derivation.pdf)**


## Schnorr signatures
https://gitlab.com/gandrewstone/compsci_461_661/-/raw/main/notes/schnorr.pdf


## Bloom, IBLT

https://gitlab.com/gandrewstone/compsci_461_661/-/raw/main/notes/bloom_iblt.pdf?inline=false
